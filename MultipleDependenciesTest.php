<?php
class MultipleDependenciesTest extends PHPUnit_Extensions_Database_TestCase
{
    // only instantiate pdo once for test clean-up/fixture load
    static private $pdo = null;

    // only instantiate PHPUnit_Extensions_Database_DB_IDatabaseConnection once per test
    private $conn = null;

    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new PDO('mysql:host=127.0.0.1;port=3307;dbname=moblyTest', "root", "");
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, "moblyTest");
        }

        return $this->conn;
    }

    public function getDataSet()
    {
        return $this->createXMLDataSet('dump.xml');
    }

    public function testGuestbook()
    {
        $ds = new PHPUnit_Extensions_Database_DataSet_QueryDataSet($this->getConnection());
        $ds->addTable('basic_sum_test');
        $bst = $ds->getTable('basic_sum_test');
        $count = $bst->getRowCount();

        for($i = 0; $i < $count; $i++){
            $row = $bst->getRow($i);

            $this->assertEquals(intval($row["x"]) + intval($row["y"]), intval($row["result"]));
        }

    }
}
?>